import FNN
import datetime
star = datetime.datetime.now()
# CREATE TRAIN DATA SET HERE
train_model = FNN.ReadData(train_data_size=600, tokenizer=True)

# CREATE NEURAL NETWORK FROM HERE
dynet_model = FNN.FNN(train_model)

# TRAIN MODEL FROM HERE
dynet_model.train(epoch_size=50)

# generate with start random word
for i in range(5):
    dynet_model.generator(dynet_model.getRandomWord(),number_of_lines=6)


results = dynet_model.get_generated_poems()


for i in results:
    print("\n*****************************************************************************************************")
    print("Result for Start Word : "+i["start word"])
    print("Perplexity " + str(i["perplexity"]))
    print("Expected Line for this sample :",i["wanted line number"])
    print("Generated Line before coming </s> token:",i["number of line produced"])
    print("Generated Poem : \n",i["generated poem"])
    print("*****************************************************************************************************\n")

print(datetime.datetime.now()-star)